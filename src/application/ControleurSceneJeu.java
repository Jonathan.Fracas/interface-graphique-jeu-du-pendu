package application;

import java.io.IOException;
import java.util.Vector;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ControleurSceneJeu {
	private GestionJeu gestionJeu;
	private GestionOption gestionOption;
	private Stage primaryStage;
	
	@FXML
	private char[] motMystere;
	
	@FXML
	private Button _button;
	
	@FXML
	private Label _labelErreurs;
	
	@FXML
	private Label _labelMotMystere;
	
	@FXML
	private ImageView _ImageView;
	
	@FXML
	private Label _VotreMot;
	
	@FXML
	private VBox _VBoxLabel;
	
	@FXML
	private VBox _VBoxClavier;
	
	@FXML
	private ToolBar _ToolBar;
	
	public ControleurSceneJeu(GestionJeu gestionJeu, GestionOption gestionOption, Stage primaryStage) {
		super();
		this.gestionJeu = gestionJeu;
		this.gestionOption = gestionOption;
		this.primaryStage = primaryStage;
		
	}
	
	@FXML
	public void initialize() throws IOException {
		_VBoxLabel.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7); -fx-background-radius: 10;");
		_VBoxClavier.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7); -fx-background-radius: 10;");
		_ToolBar.setStyle("-fx-background-color: rgba(8, 166, 2, 0.4);");
		motMystere = new char[gestionOption.getNombreLettres()];
		
		_labelErreurs.setText("Vous avez fait "+gestionJeu.getNbErreurs()+" erreur(s) sur 4 possibles");
		
		for(int i=0; i<gestionOption.getNombreLettres();i++) {
			motMystere[i] = '_';
		}
		//System.out.println(gestionJeu.getMotMystere());
		_labelMotMystere.setText(String.valueOf(motMystere));
		
		if(gestionOption.getIndiceImages()==0) {
			_ImageView.setImage(new Image("penduHommeBase.jpg"));
		}
		else {
			_ImageView.setImage(new Image("penduFemmeBase.jpg"));
		}
	}
	
	

	@FXML
	public void cliqueClavierVirtuel(ActionEvent event) throws IOException {
		Button buttonClic = (Button) (event.getSource());
		String lettreSelectionnee = buttonClic.getText();
		Vector<Integer> pos = new Vector<Integer>();
        if (gestionJeu.ChercherLettreDansMot(lettreSelectionnee.charAt(0), pos) == 0)
        {	
        	gestionJeu.MAJNbErreurs();
             if (gestionJeu.MaxErreursDepasse())
             {
            	 changementImagePendu(gestionJeu.getNbErreurs());
            	 _labelErreurs.setText("Vous avez fait "+gestionJeu.getNbErreurs()+" erreur(s) sur 4 possibles");
            	 changementSceneFinJeu();
             }
             else
            	 changementImagePendu(gestionJeu.getNbErreurs());
            	 _labelErreurs.setText("Vous avez fait "+gestionJeu.getNbErreurs()+" erreur(s) sur 4 possibles");
        }
        else
        {	
     	   	if (gestionJeu.ToutTrouve()) 
             {	
     	   		motMystere[pos.firstElement()]=lettreSelectionnee.charAt(0);
     	   	    _labelMotMystere.setText(String.valueOf(motMystere));
     	   	    changementSceneFinJeu();
             }
             else
             {
            	 int cpt = pos.size();
            	 for(int i=0; i<cpt;i++) {	
            		 motMystere[pos.firstElement()]=lettreSelectionnee.charAt(0);
            		 pos.remove(0);
            	 }
            	 _labelMotMystere.setText(String.valueOf(motMystere));       
        }}
        buttonClic.setDisable(true);
	}
	
	private void changementImagePendu(int nbErreurs) {
		if(gestionOption.getIndiceImages()==0) {
			 switch(nbErreurs){
		       case 1: 
					_ImageView.setImage(new Image("penduHomme1.jpg"));
		           break;
		       case 2:
		    	   _ImageView.setImage(new Image("penduHomme2.jpg"));
		           break;
		       case 3:
		    	   _ImageView.setImage(new Image("penduHomme3.jpg"));
		           break;
		       case 4:
		    	   _ImageView.setImage(new Image("penduHomme4.jpg"));
		           break;
		       case 5:
		    	   _ImageView.setImage(new Image("penduHomme.jpg"));
		           break;
		       default:
		           System.out.println("pbm");
		           break;
		   }
		}
		else {
			switch(nbErreurs){
		       case 1: 
					_ImageView.setImage(new Image("penduFemme1.jpg"));
		           break;
		       case 2:
		    	   _ImageView.setImage(new Image("penduFemme2.jpg"));
		           break;
		       case 3:
		    	   _ImageView.setImage(new Image("penduFemme3.jpg"));
		           break;
		       case 4:
		    	   _ImageView.setImage(new Image("penduFemme4.jpg"));
		           break;
		       case 5:
		    	   _ImageView.setImage(new Image("penduFemme.jpg"));
		           break;
		       default:
		           System.out.println("pbm");
		           break;
		   }
		}
	}

	public void changementSceneFinJeu() throws IOException {
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("VueSceneFin.fxml"));
        ControleurSceneFin controleurSceneFin = new ControleurSceneFin(gestionJeu,gestionOption,primaryStage);
        loader.setController(controleurSceneFin);
        
        VBox vbox = loader.load();
        Scene scene = new Scene(vbox,500,300);
        
        Image image = new Image("backgroundIntro.jpg");
        BackgroundImage backgroundImage = new BackgroundImage(image,
                                                   BackgroundRepeat.NO_REPEAT,
                                                   BackgroundRepeat.NO_REPEAT,
                                                   BackgroundPosition.DEFAULT,
                                                   BackgroundSize.DEFAULT);
        Background backGround = new Background(backgroundImage);
        vbox.setBackground(backGround);
        
        primaryStage.setScene(scene);
	}
	
	@FXML
	public void cliqueSurJouer() throws IOException{
		BarreOutil.cliqueSurJouer(gestionJeu,gestionOption,primaryStage);
	}
	
	@FXML
	public void cliqueSurOptions() throws IOException{
		BarreOutil.cliqueSurOptions(gestionJeu,gestionOption,primaryStage);
	}
	
	@FXML
	public void cliqueSurRegles() throws IOException{
		BarreOutil.cliqueSurRegles();
	}
	
	@FXML
	public void cliqueSurQuitter() throws IOException{
		BarreOutil.cliqueSurQuitter();
	}
}
