package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;


public class Main extends Application {
	private GestionJeu gestionJeu;
	private GestionOption gestionOption;

	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(getClass().getResource("VueSceneIntro.fxml"));
	        
	        ControleurSceneIntro controleurSceneIntro = new ControleurSceneIntro(gestionJeu,gestionOption,primaryStage);
	        loader.setController(controleurSceneIntro);
			
			VBox root = loader.load();
			Scene scene = new Scene(root,500,300);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			
			Image image = new Image("backgroundIntro.jpg");
	        BackgroundImage backgroundImage = new BackgroundImage(image,
	                                                   BackgroundRepeat.NO_REPEAT,
	                                                   BackgroundRepeat.NO_REPEAT,
	                                                   BackgroundPosition.DEFAULT,
	                                                   BackgroundSize.DEFAULT);
	        Background backGround = new Background(backgroundImage);
	        root.setBackground(backGround);
			
			
			primaryStage.setScene(scene);
			
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void init() throws IOException {
		gestionOption = new GestionOption();
		gestionJeu = new GestionJeu("Dictionnaires/Dico4Lettres.txt");
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
