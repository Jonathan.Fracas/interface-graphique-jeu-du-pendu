package application;

import java.io.IOException;
import java.net.URISyntaxException;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * Controleur de la scene de fin.
 *
 */
public class ControleurSceneFin {
	private GestionJeu gestionJeu;
	private GestionOption gestionOption;
	private Stage primaryStage;
	
	@FXML
	private ImageView _imageView;
	
	@FXML
	private Label _labelFin;
	
	@FXML
	private ToolBar _ToolBar;
	
	
	public ControleurSceneFin(GestionJeu gestionJeu, GestionOption gestionOption, Stage primaryStage) {
		super();
		this.gestionJeu = gestionJeu;
		this.gestionOption = gestionOption;
		this.primaryStage = primaryStage;
	}
	
	/**
	 * Selon le resultat de la partie, on affiche une image et un label.
	 * 
	 * @throws URISyntaxException
	 */
	@FXML
	public void initialize() throws URISyntaxException {
		_labelFin.setStyle("-fx-background-color: rgba(255, 255, 255, 0.7); -fx-background-radius: 5;");
		_ToolBar.setStyle("-fx-background-color: rgba(8, 166, 2, 0.4);");
		if(gestionJeu.ToutTrouve()) {
			Image victoire= new Image("victoire.png");
			_imageView.setImage(victoire);
			_labelFin.setText("Felicitations vous avez trouvez le mot : "+gestionJeu.getMotMystere());
		}
		else {
			Image defaite= new Image("game over.png");
			_imageView.setImage(defaite);
			_labelFin.setText("Dommage vous n'avez pas trouvez le mot : "+gestionJeu.getMotMystere());
		}
	}
	
	@FXML
	public void cliqueSurJouer() throws IOException{
		BarreOutil.cliqueSurJouer(gestionJeu,gestionOption,primaryStage);
	}
	
	@FXML
	public void cliqueSurOptions() throws IOException{
		BarreOutil.cliqueSurOptions(gestionJeu,gestionOption,primaryStage);
	}
	
	@FXML
	public void cliqueSurRegles() throws IOException{
		BarreOutil.cliqueSurRegles();
	}
	
	@FXML
	public void cliqueSurQuitter() throws IOException{
		BarreOutil.cliqueSurQuitter();
	}
}
