package application;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * 
 * Controleur de la scene d'intro.
 *
 */
public class ControleurSceneIntro {
	private GestionJeu gestionJeu;
	private GestionOption gestionOption;
	private Stage primaryStage;
	
	public ControleurSceneIntro(GestionJeu gestionJeu, GestionOption gestionOption, Stage primaryStage) {
		super();
		this.gestionJeu = gestionJeu;
		this.gestionOption = gestionOption;
		this.primaryStage = primaryStage;
	}
	
	@FXML
	private void initialize(){
	}
	
	@FXML
	public void cliqueSurJouer() throws IOException{
		BarreOutil.cliqueSurJouer(gestionJeu,gestionOption,primaryStage);
	}
	
	@FXML
	public void cliqueSurOptions() throws IOException{
		BarreOutil.cliqueSurOptions(gestionJeu,gestionOption,primaryStage);
	}
	
	@FXML
	public void cliqueSurRegles() throws IOException{
		BarreOutil.cliqueSurRegles();
	}
	
	@FXML
	public void cliqueSurQuitter() throws IOException{
		BarreOutil.cliqueSurQuitter();
	}
}
