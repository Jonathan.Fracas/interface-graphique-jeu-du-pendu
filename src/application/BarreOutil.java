package application;

import java.io.IOException;
import java.util.Optional;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Classe contenant les m�thodes utilis�es par la barre d'outil
 *
 */
public class BarreOutil {
	
	/**
	 * Initialise une partie et affiche la scene de jeu.
	 * 
	 * @param gestionJeu
	 * @param gestionOption
	 * @param primaryStage
	 * @throws IOException
	 */
	public final static void cliqueSurJouer(GestionJeu gestionJeu,GestionOption gestionOption, Stage primaryStage) throws IOException{
		gestionJeu.InitialiserPartie();
		
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(BarreOutil.class.getResource("VueSceneJeu.fxml"));
        ControleurSceneJeu controleurSceneJeu = new ControleurSceneJeu(gestionJeu,gestionOption,primaryStage);
        loader.setController(controleurSceneJeu);
        
        VBox vbox = loader.load();
        Scene scene = new Scene(vbox,500,300);
        
        Image image = new Image("backgroundIntro.jpg");
        BackgroundImage backgroundImage = new BackgroundImage(image,
                                                   BackgroundRepeat.NO_REPEAT,
                                                   BackgroundRepeat.NO_REPEAT,
                                                   BackgroundPosition.DEFAULT,
                                                   BackgroundSize.DEFAULT);
        Background backGround = new Background(backgroundImage);
        vbox.setBackground(backGround);        
        primaryStage.setScene(scene);        
	}
	
	/**
	 * Ouvre une boite de dialogue contenant les 3 options modifiables :
	 * - taille de la police.
	 * - l'image du pendu.
	 * - la taille des mots.
	 * 
	 * @param gestionJeu
	 * @param gestionOption
	 * @param primaryStage
	 * @throws IOException
	 */
	public final static void cliqueSurOptions(GestionJeu gestionJeu,GestionOption gestionOption, Stage primaryStage) throws IOException{		 
		FXMLLoader loader = new FXMLLoader();
        loader.setLocation(BarreOutil.class.getResource("VueBoiteOptions.fxml"));
        ControleurBoiteOptions controlleurBoiteOptions = new ControleurBoiteOptions(gestionOption);
		loader.setController(controlleurBoiteOptions);
		DialogPane dialogPane = loader.load();
		
		Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("Options");
        dialog.setDialogPane(dialogPane);
        
        dialogPane.setMinWidth(400);
        
        Optional<ButtonType> clickButton = dialog.showAndWait();

        if(clickButton.get()==ButtonType.OK) {
        	switch(controlleurBoiteOptions.choixNombreLettres()){
		       case -1: 
		           break;
		       case 4:
		    	   gestionOption.setNombreLettres(controlleurBoiteOptions.choixNombreLettres());
		    	   gestionJeu.ChangerDico("Dictionnaires/Dico4Lettres.txt");
		           break;
		       case 6:
		    	   gestionOption.setNombreLettres(controlleurBoiteOptions.choixNombreLettres());
		    	   gestionJeu.ChangerDico("Dictionnaires/Dico6Lettres.txt");
		           break;
		       case 8:
		    	   gestionOption.setNombreLettres(controlleurBoiteOptions.choixNombreLettres());
		    	   gestionJeu.ChangerDico("Dictionnaires/Dico8Lettres.txt");
		           break;
		       default:
		           System.out.println("pbm");
		           break;
		   }
        	gestionOption.setIndiceImages(controlleurBoiteOptions.choixImagePendu());
        	gestionOption.setTaillePolice(controlleurBoiteOptions.choixTaillePolice());
        }
	}
	
	/**
	 * Affiche la liste des regles dans une boite de dialogue.
	 * 
	 * @throws IOException
	 */
	public final static void cliqueSurRegles() throws IOException{
		Alert dialog = new Alert(AlertType.INFORMATION);
		dialog.setTitle("R�gles");
		dialog.setHeaderText("Liste des r�gles du jeu");
		dialog.setContentText("Lorsque vous lancez une partie un mot al�atoire est choisie"
				+ " votre objectif est de trouver ce mot.\n" +
				"Pour cela, vous devez cliquez sur une des lettres du clavier apparaissant sur votre �cran.\n"+
				"Maintenant il y a deux cas de figures possibles : \n"+
				"- le mot contient la lettre choisie : dans ce cas la lettre apparait sur votre �cran � sa ou ses positions dans le mot.\n"+
				"- le mot ne contient pas la lettre choisie : dans ce cas vous avez fait une erreur.\n"+
				"Si vous faites plus de 4 erreurs, vous perdez la partie.");
		dialog.initModality(Modality.NONE);
		dialog.getDialogPane().setMinHeight(300);
		dialog.getDialogPane().setMinWidth(400);
		dialog.show();
	}
	
	/**
	 * Quitte l'application.
	 * 
	 * @throws IOException
	 */
	public final static void cliqueSurQuitter() throws IOException{
		System.exit(0);
	}
}
