package application;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.ToggleGroup;
import javafx.scene.text.Font;

/**
 * Controleur de la boite de dialogue du choix des options.
 *
 */
public class ControleurBoiteOptions {
	private GestionOption gestionOption; 
	
	private ToggleGroup radioGroup = new ToggleGroup();
	
	@FXML
	private RadioButton _choixHomme;
	
	@FXML
	private RadioButton _choixFemme;
	
	@FXML
    private ChoiceBox<String> _nombreLettres;
	
	@FXML
	private Spinner<Integer> _taillePolice;
	
	@FXML
	private Label _previsualisation;
	
	public ControleurBoiteOptions(GestionOption gestionOption) {
		this.gestionOption = gestionOption;
	}
	

	@FXML
	public void initialize() {
		// initialisation du RadioGroup pour le choix de l'image du pendu.
		_choixFemme.setToggleGroup(radioGroup);
		_choixHomme.setToggleGroup(radioGroup);
		radioGroup.selectToggle(_choixHomme);
		
		// initialisation de la ChoiceBox pour le choix de la difficultee.
		_nombreLettres.getItems().add("4");
		_nombreLettres.getItems().add("6");
		_nombreLettres.getItems().add("8");
		
		// initialisation du Spinner pour le choix de la taille de la police et liaison avec 
		// la previsualisation.
		SpinnerValueFactory<Integer> valueFactory = 
				new SpinnerValueFactory.IntegerSpinnerValueFactory(8,50);
		valueFactory.setValue(gestionOption.getTaillePolice());
		
		_taillePolice.setValueFactory(valueFactory);
		_previsualisation.setFont(new Font((double) _taillePolice.getValue()));
		
		_taillePolice.valueProperty().addListener(new ChangeListener<Integer>() {
			@Override
			public void changed(ObservableValue<? extends Integer> arg0, Integer arg1, Integer arg2) {
				_previsualisation.setFont(new Font((double) _taillePolice.getValue()));
			}
		});
	}
	
	
	public int choixTaillePolice() {		
		return _taillePolice.getValue();
	}
	
	
	public int choixNombreLettres() {
		if(_nombreLettres.getSelectionModel().getSelectedItem()!=null) {
			return Integer.parseInt(_nombreLettres.getSelectionModel().getSelectedItem());
		}
		else {
			return -1;
		}
	}
	
	public int choixImagePendu() {
		if (radioGroup.getSelectedToggle()==_choixHomme) {
			return 0;
		}
		else {
			return 1;
		}
	}
	
}
