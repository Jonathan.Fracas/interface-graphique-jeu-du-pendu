package application;

public class GestionOption {
	
	private int nombreLettres=4;
	private int indiceImages=0;
	private int taillePolice=14;
	
	public GestionOption() {
	}
	
	public int getNombreLettres() {
		return nombreLettres;
	}
	public void setNombreLettres(int nombreLettres) {
		this.nombreLettres = nombreLettres;
	}
	public int getIndiceImages() {
		return indiceImages;
	}
	public void setIndiceImages(int indiceImages) {
		this.indiceImages = indiceImages;
	}
	public int getTaillePolice() {
		return taillePolice;
	}
	public void setTaillePolice(int taillePolice) {
		this.taillePolice = taillePolice;
	}
	
	
}
